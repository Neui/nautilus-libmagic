#include "info-provider.h"
#include "column-provider.h"
//#include <glib/gi18n-lib.h>

#include <libnautilus-extension/nautilus-extension-types.h>

static GType type_list[2];

void
nautilus_module_initialize(GTypeModule* module)
{
    nautilus_libmagic_info_provider_load(module);
    nautilus_libmagic_column_provider_load(module);

    type_list[0] = NAUTILUS_LIBMAGIC_TYPE_INFO_PROVIDER;
    type_list[1] = NAUTILUS_LIBMAGIC_TYPE_COLUMN_PROVIDER;

    /* bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR); */
    /* bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8"); */
}

void
nautilus_module_shutdown(void)
{
}

void
nautilus_module_list_types(const GType** types, int* num_types)
{
    *types = type_list;
    *num_types = G_N_ELEMENTS(type_list);
}
