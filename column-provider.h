#include <glib-object.h>

G_BEGIN_DECLS

#define NAUTILUS_LIBMAGIC_TYPE_COLUMN_PROVIDER (nautilus_libmagic_column_provider_get_type())
G_DECLARE_FINAL_TYPE(NautilusLibmagicColumnProvider,
  nautilus_libmagic_column_provider,
  NAUTILUS_LIBMAGIC,
  COLUMN_PROVIDER,
  GObject)

void
nautilus_libmagic_column_provider_load(GTypeModule* module);

G_END_DECLS
