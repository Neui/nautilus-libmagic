#include "info-provider.h"

#include <magic.h>

#include <errno.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <gio/gio.h>
#include <glib.h>
//#include <glib/gi18n-lib.h>

#include <libnautilus-extension/nautilus-info-provider.h>

#define DEFAULT_FILE_PEEK (512 * 2 * 4)

// TODO: Maybe wrap the libmagic cookie into a gobject

// Currently never seen outside of this file
#define NAUTILUS_LIBMAGIC_GENERIC_ERROR nautilus_libmagic_generic_error_quark()
GQuark
nautilus_libmagic_generic_error_quark(void)
{
    return g_quark_from_static_string("nautilus-libmagic-generic-error-quark");
}

struct _NautilusLibmagicInfoProvider
{
    GObject parent_instance;
    magic_t magic_desc;
    magic_t magic_mime;
};

// The send-to extension does this for some reason
static void
info_provider_iface_init(NautilusInfoProviderIface* iface);

G_DEFINE_DYNAMIC_TYPE_EXTENDED(NautilusLibmagicInfoProvider,
  nautilus_libmagic_info_provider,
  G_TYPE_OBJECT,
  0,
  G_IMPLEMENT_INTERFACE_DYNAMIC(NAUTILUS_TYPE_INFO_PROVIDER, info_provider_iface_init))

void
ipt_callback(GObject* source_object, GAsyncResult* res, gpointer user_data)
{
    if (!g_task_is_valid(G_TASK(res), source_object))
    {
        g_error("Expected an correct GTask, but apparantly, it isn't: %p", (void*)res);
        return;
    }
    else if (!NAUTILUS_LIBMAGIC_IS_INFO_PROVIDER(source_object))
    {
        g_error("Expected an NautilusLibmagicInfoProvider, but apparantly, it isn't: %p",
          (void*)source_object);
        return;
    }

    NautilusFileInfo* file_info = NAUTILUS_FILE_INFO(g_task_get_task_data(G_TASK(res)));
    g_debug("Notifying for %s", nautilus_file_info_get_uri(file_info));

    GError* err = NULL;
    g_task_propagate_boolean(G_TASK(res), &err);
    if (err)
    {
        g_info("Error: %s (for %s)", err->message, nautilus_file_info_get_uri(file_info));
    }

    GClosure* update_complete = (GClosure*)user_data;
    if (!g_cancellable_is_cancelled(g_task_get_cancellable(G_TASK(res))))
    {
        nautilus_info_provider_update_complete_invoke(update_complete,
          NAUTILUS_INFO_PROVIDER(source_object),
          (NautilusOperationHandle*)res,
          err ? NAUTILUS_OPERATION_FAILED : NAUTILUS_OPERATION_COMPLETE);
        /* Hack?: Invalidate the info in order to use the values from cache.
         * This somehow makes Nautilus display the info. Not sure why...
         * It seems when returning NAUTILUS_OPERATION_IN_PROGRESS,
         * you can see the correct values briefly (if lucky), but then changes
         * to "unknown" again. In one case/directory, a refresh (F5) was
         * required to display it properly, so this is where I got that idea. */
        nautilus_file_info_invalidate_extension_info(
          (NautilusFileInfo*)g_task_get_task_data(G_TASK(res)));
    }

    g_error_free(err);
    g_closure_unref(update_complete);
}

static void
ipt_thread(GTask* task, gpointer source_object, gpointer task_data, GCancellable* cancellable)
{
    if (!g_task_is_valid(task, source_object))
    {
        g_error("Expected an correct GTask, but apparantly, it isn't: %p", (void*)task);
        return;
    }
    else if (!NAUTILUS_LIBMAGIC_IS_INFO_PROVIDER(source_object))
    {
        g_error("Expected an NautilusLibmaicInfoProvider, but apparantly, it isn't: %p",
          (void*)task_data);
        return;
    }
    else if (!NAUTILUS_IS_FILE_INFO(task_data))
    {
        g_error("Expected an NautilusFileInfo, but apparantly, it isn't: %p", (void*)task_data);
        return;
    }
    else if (!G_IS_CANCELLABLE(cancellable))
    {
        g_error("Expected an GCancellable, but apparantly, it isn't: %p", (void*)cancellable);
        return;
    }

    char buffer[DEFAULT_FILE_PEEK];
    GError* err = NULL;
    NautilusLibmagicInfoProvider* provider = NAUTILUS_LIBMAGIC_INFO_PROVIDER(source_object);
    NautilusFileInfo* file_info = NAUTILUS_FILE_INFO(task_data);

    GFile* file = nautilus_file_info_get_location(file_info);

    GFileInputStream* filestream = g_file_read(file, cancellable, &err);
    g_object_unref(file);
    if (err)
    {
        g_object_unref(filestream);
        g_task_return_error(task, err);
        return;
    }

    gssize size =
      g_input_stream_read(G_INPUT_STREAM(filestream), buffer, DEFAULT_FILE_PEEK, cancellable, &err);
    if (err)
    {
        g_object_unref(filestream);
        g_task_return_error(task, err);
        return;
    }

    /* Don't care about any errors, for now */
    g_input_stream_close(G_INPUT_STREAM(filestream), NULL, NULL);
    g_object_unref(filestream);

    const char* normal = magic_buffer(provider->magic_desc, buffer, size);
    const char* mime = magic_buffer(provider->magic_mime, buffer, size);

    if (!normal)
    {
        g_warning("libmagic (desc) error for %s: %s",
          nautilus_file_info_get_uri(file_info),
          magic_error(provider->magic_desc));
    }
    if (!mime)
    {
        g_warning("libmagic (MIME) error for %s: %s",
          nautilus_file_info_get_uri(file_info),
          magic_error(provider->magic_mime));
    }
    if (!normal && !mime)
    {
        g_task_return_new_error(task,
          nautilus_libmagic_generic_error_quark(),
          1,
          "libmagic returned NULL for both of them!");
        return;
    }

    g_debug("libmagic says: %s (%s) for %s", normal, mime, nautilus_file_info_get_uri(file_info));
    nautilus_file_info_add_string_attribute(file_info, "libmagic::description", normal);
    g_object_set_data_full(G_OBJECT(file_info), "libmagic_description", g_strdup(normal), g_free);

    nautilus_file_info_add_string_attribute(file_info, "libmagic::mime_type", mime);
    g_object_set_data_full(G_OBJECT(file_info), "libmagic_mime_type", g_strdup(mime), g_free);

    g_task_return_boolean(task, TRUE);
}

NautilusOperationResult
nautilus_libmagic_info_provider_update_file_info(NautilusInfoProvider* provider,
  NautilusFileInfo* file,
  GClosure* update_complete,
  NautilusOperationHandle** handle)
{
    NautilusLibmagicInfoProvider* nl = NAUTILUS_LIBMAGIC_INFO_PROVIDER(provider);

    char* desc = g_object_get_data(G_OBJECT(file), "libmagic_description");
    char* mime = g_object_get_data(G_OBJECT(file), "libmagic_mime_type");

    if (desc)
    {
        g_debug("Using cached desc for: %s", nautilus_file_info_get_uri(file));
        nautilus_file_info_add_string_attribute(file, "libmagic::description", desc);
    }
    if (mime)
    {
        g_debug("Using cached mime type for: %s", nautilus_file_info_get_uri(file));
        nautilus_file_info_add_string_attribute(file, "libmagic::mime_type", mime);
    }
    /* Using || in case on of them "fails"/doesn't contain data to prevent looping */
    if (desc || mime)
        return NAUTILUS_OPERATION_COMPLETE;

    if (nautilus_file_info_is_directory(file))
    {
        g_debug("Using hard-coded values for directory: %s", nautilus_file_info_get_uri(file));
        nautilus_file_info_add_string_attribute(file, "libmagic::description", "directory");
        nautilus_file_info_add_string_attribute(file, "libmagic::mime_type", "inode/directory");
        return NAUTILUS_OPERATION_COMPLETE;
    }
    g_debug("Starting async task for: %s", nautilus_file_info_get_uri(file));

    GTask* task = g_task_new(nl, g_cancellable_new(), ipt_callback, g_closure_ref(update_complete));
    g_task_set_task_data(task, g_object_ref(file), (GDestroyNotify)g_object_unref);
    g_task_set_check_cancellable(task, TRUE);
    g_task_run_in_thread(task, ipt_thread);
    g_object_unref(task);
    /* NautilusOperationHandle* isn't very clear for me, but it seems it is
     * just an void*, something that can then be referenced later for
     * cancelling or IDing the file/process. */
    *handle = (NautilusOperationHandle*)task;

    return NAUTILUS_OPERATION_IN_PROGRESS;
}

void
nautilus_libmagic_info_provider_cancel_update(NautilusInfoProvider* provider,
  NautilusOperationHandle* handle)
{
    (void)provider;
    GTask* task = (GTask*)handle;
    g_debug(
      "Cancelling %s", nautilus_file_info_get_uri(NAUTILUS_FILE_INFO(g_task_get_task_data(task))));
    g_cancellable_cancel(g_task_get_cancellable(task));
}

static void
info_provider_iface_init(NautilusInfoProviderIface* iface)
{
    iface->update_file_info = nautilus_libmagic_info_provider_update_file_info;
    iface->cancel_update = nautilus_libmagic_info_provider_cancel_update;
}

static void
nautilus_libmagic_info_provider_init(NautilusLibmagicInfoProvider* nl)
{
    /* TODO: Check if magic_load actually sets errno, since that isn't
     * really written in the man page */
    errno = 0;
    if (!(nl->magic_desc = magic_open(MAGIC_PRESERVE_ATIME)))
    {
        g_warning("Creating magic desc object failed! %s", strerror(errno));
    }
    else if (magic_load(nl->magic_desc, NULL) < 0)
    {
        g_warning("Loading magic database for desc failed! %s", strerror(errno));
    }

    errno = 0;
    if (!(nl->magic_mime = magic_open(MAGIC_PRESERVE_ATIME | MAGIC_MIME_TYPE)))
    {
        g_warning("Creating magic mime object failed! %s", strerror(errno));
    }
    else if (magic_load(nl->magic_mime, NULL) < 0)
    {
        g_warning("Loading magic database for mime failed! %s", strerror(errno));
    }

    g_debug(
      "Called provider init! desc: %p; mime: %p", (void*)nl->magic_desc, (void*)nl->magic_mime);
}

static void
nautilus_libmagic_info_provider_finalize(GObject* gobject)
{
    NautilusLibmagicInfoProvider* nl = NAUTILUS_LIBMAGIC_INFO_PROVIDER(gobject);

    if (nl->magic_desc)
        magic_close(nl->magic_desc);
    if (nl->magic_mime)
        magic_close(nl->magic_mime);

    G_OBJECT_CLASS(nautilus_libmagic_info_provider_parent_class)->finalize(gobject);
}

static void
nautilus_libmagic_info_provider_class_init(NautilusLibmagicInfoProviderClass* klass)
{
    GObjectClass* object_class = G_OBJECT_CLASS(klass);
    object_class->finalize = nautilus_libmagic_info_provider_finalize;
}

static void
nautilus_libmagic_info_provider_class_finalize(NautilusLibmagicInfoProviderClass* klass)
{
    (void)klass;
}

void
nautilus_libmagic_info_provider_load(GTypeModule* module)
{
    nautilus_libmagic_info_provider_register_type(module);
}
