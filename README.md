`libnautilus-libmagic`
======================

An [nautilus (GNOME Files)][nautilus] extension to integrate
[`libmagic`][libmagic] descriptions (like the ones provided by the
[file(1)][file1] utility) into a column.

Building
--------

To build, you need

* CMake 3.10+ and a build system such as `make`
* An C compiler, such as `gcc` or `clang`
* `libnautilus-extension-dev`
* `libmagic-dev`

To build in the source directory, do:

	cmake . -DCMAKE_INSTALL_PREFIX=/usr && cmake --build .

This should generate an `libnautilus-libmagic.so` file to place it into
`/usr/lib/nautilus/extensions-3.0/`, and then restart Nautilus completely.

The reason why you need to use `/usr` as the install prefix instead of
`/usr/local` is that it seems nautilus doesn't look for extensions in
`/usr/local` by default.

Thanks to
---------

* The one who wrote the [Nautilus Extension][tt_nautilusextension]
* Contributers of Nautilus for making the code available
* Contributors of the 'default' extensions (like send-to)
* Contributors of the (albet slim) [libnautilus-extension documentation][tt_lne]

[nautilus]: https://wiki.gnome.org/Apps/Files
[libmagic]: https://web.archive.org/web/20190924194201/http://darwinsys.com/file/
[file1]: https://manpages.debian.org/buster/file/file.1.en.html
[tt_nautilusextension]: https://web.archive.org/web/20090418175132/http://www.campd.org/stuff/docs/extending-nautilus/NautilusExtensions.html 
[tt_lne]: https://developer.gnome.org/libnautilus-extension/3.24/ch01.html

