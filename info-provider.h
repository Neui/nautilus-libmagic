#include <glib-object.h>

G_BEGIN_DECLS

#define NAUTILUS_LIBMAGIC_TYPE_INFO_PROVIDER (nautilus_libmagic_info_provider_get_type())
G_DECLARE_FINAL_TYPE(NautilusLibmagicInfoProvider,
  nautilus_libmagic_info_provider,
  NAUTILUS_LIBMAGIC,
  INFO_PROVIDER,
  GObject)

void
nautilus_libmagic_info_provider_load(GTypeModule* module);

G_END_DECLS
