cmake_minimum_required(VERSION 3.10 FATAL_ERROR)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}")

project(nautilus-libmagic C)
set(NAUTILUS_VERSION 3.0)

include(FindPkgConfig)
find_package(libmagic REQUIRED)
pkg_check_modules(glib-2.0 REQUIRED glib-2.0 IMPORTED_TARGET)
pkg_check_modules(gio-2.0 REQUIRED gio-2.0 IMPORTED_TARGET)
pkg_check_modules(libnautilus-extension REQUIRED libnautilus-extension IMPORTED_TARGET)

add_library(nautilus-libmagic SHARED
	main.c
	info-provider.c
	column-provider.c
	)
target_link_libraries(nautilus-libmagic
	libmagic
	PkgConfig::glib-2.0
	PkgConfig::gio-2.0
	PkgConfig::libnautilus-extension
	)
target_compile_definitions(nautilus-libmagic PRIVATE "-DG_LOG_DOMAIN=\"nautilus-libmagic\"")
set_property(TARGET nautilus-libmagic PROPERTY C_STANDARD 99)

include(GNUInstallDirs)
install(TARGETS nautilus-libmagic
	LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}/nautilus/extensions-${NAUTILUS_VERSION}"
	)

