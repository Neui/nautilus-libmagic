
#include "column-provider.h"

#include <glib-object.h>
#include <string.h>
//#include <glib/gi18n-lib.h>

#include <libnautilus-extension/nautilus-column-provider.h>

struct _NautilusLibmagicColumnProvider
{
    GObject parent_instance;
};

// The send-to extension does this for some reason
static void
column_provider_iface_init(NautilusColumnProviderIface* iface);

G_DEFINE_DYNAMIC_TYPE_EXTENDED(NautilusLibmagicColumnProvider,
  nautilus_libmagic_column_provider,
  G_TYPE_OBJECT,
  0,
  G_IMPLEMENT_INTERFACE_DYNAMIC(NAUTILUS_TYPE_COLUMN_PROVIDER, column_provider_iface_init))
GList*
get_columns(NautilusColumnProvider* provider)
{
    (void)provider;
    GList* columns = NULL;
    columns = g_list_prepend(columns,
      nautilus_column_new("libmagic::description_column",
        "libmagic::description",
        "Magic Description",
        "More accurate description by using a file magic bytes (see file(1))"));
    columns = g_list_prepend(columns,
      nautilus_column_new("libmagic::mime_type_column",
        "libmagic::mime_type",
        "Magic MIME",
        "More accurate MIME type by using a file magic bytes (see file(1))"));
    g_debug("Returning columns descriptions! %p", (void*)columns);
    return columns;
}

static void
column_provider_iface_init(NautilusColumnProviderIface* iface)
{
    iface->get_columns = get_columns;
}

static void
nautilus_libmagic_column_provider_init(NautilusLibmagicColumnProvider* nl)
{
    (void)nl;
}

static void
nautilus_libmagic_column_provider_class_init(NautilusLibmagicColumnProviderClass* klass)
{
    (void)klass;
}

static void
nautilus_libmagic_column_provider_class_finalize(NautilusLibmagicColumnProviderClass* klass)
{
    (void)klass;
}

void
nautilus_libmagic_column_provider_load(GTypeModule* module)
{
    nautilus_libmagic_column_provider_register_type(module);
}
